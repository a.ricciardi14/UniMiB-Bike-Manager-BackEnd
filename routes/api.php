<?php

use Illuminate\Http\Request;



Route::get('racks', 'RackController@index');
Route::get('racks/{id}', 'RackController@getRack');
Route::get('rentals', 'RentalController@index');
Route::get('bikes/{id}' , 'BikeController@getBike');
Route::get('buildings', 'BuildingController@index');
Route::get('reservations', 'ReservationController@index');
Route::post('reports', 'ReportController@store');
Route::post('rentals', 'RentalController@storeRental');
Route::put('rentals/{id}', 'RentalController@closeRental');

Route::get('userId', 'UserController@index');
Route::post('login', 'UserController@authUser');
Route::post('addUser', 'UserController@storeUser');

Route::get('reports/{id}/image', 'ReportController@getImage');


//Route create
Route::get('bikes', 'BikeController@getBikeList');
Route::delete('bikes/{id}', 'BikeController@deleteBike');
Route::delete('racks/{id}', 'RackController@deleteRack');

Route::get('reports', 'ReportController@getAllReports');


// //NON FUNZIONA
//Route::put('bikes/{id}?{request}', 'BikeController@changeDisp');
//Route::post('bikes', 'BikeController@storeBike');
// Route::post('racks/{id}?{request}', 'RackController@storeRack');


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
