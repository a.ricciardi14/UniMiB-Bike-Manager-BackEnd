<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
	public $timestamps = false;

    public static function boot(){
        parent::boot();
        static::creating(function ($model) {
            $model->created_on = $model->freshTimestamp();
        });
    }

    public function bike(){
    	return $this->belongsTo('App\Bike');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

}
