<?php

namespace App\Repositories;

use App\Repositories\RackRepository;
use Illuminate\Support\Facades\DB;

class PositionService {

  private function distance($lat, $long, $lat2, $long2) {

  $theta = $long - $long2;
  $dist = sin(deg2rad($lat)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;

  return $distance = ($miles * 1.609344);
   
  }

  private function order($ris) {
    usort($ris, function($a, $b) {
    return $a['distance'] <=> $b['distance'];
    });
    return $ris;
  }

  public function calculate($array, $lat, $long) {
     
   $ris = array(); 
   $ord = array();

      foreach ($array as $i) {
        $lat2 = $i->latitude;
        $long2 = $i->longitude;
 
        $dist = self::distance($lat,$long,$lat2,$long2);
		
        $i->distance = round($dist, 2);
        $ris[] = $i;  
      }
      $ord = self::order($ris);
      return $ord;
  }

  public function calculateMaxDist($array, $lat, $long, $max_distance) {
     
   $ris = array(); 
   $ord = array();

      foreach ($array as $i) {
        $lat2 = $i->latitude;
        $long2 = $i->longitude;
 
        $dist = self::distance($lat,$long,$lat2,$long2);
        $i->distance = $dist;

        if($dist <= $max_distance){
          $ris[] = $i;
        }  
      }
      $ord = self::order($ris);
      return $ord;
  }


}
