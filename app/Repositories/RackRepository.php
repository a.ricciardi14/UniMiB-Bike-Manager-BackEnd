<?php

namespace App\Repositories;

use App\Rack;
use App\Building;
use Illuminate\Support\Facades\DB;

class RackRepository {

	public function findAllRacks() {
		return Rack::with('buildings')->get();
	}

	public function findRacksByAvailableStands($available_stands){
		return Rack::where('available_stands', '>=', $available_stands)->with('buildings')->get();
	}

	public function findRacksByAvailableBikes($available_bikes){
		return Rack::where('available_bikes', '>=', $available_bikes)->with('buildings')->get();
	}

	public function findRacksByBuildingId($building_id){
		return Building::findOrFail($building_id)->racks()->with('buildings')->get();

	}

	public function findRackById($id){
		return Rack::with('buildings')->findOrFail($id);
	}

	public function findRacksList(){
		$array = Rack::with('buildings')->get();
		return $array;
	}

	//-------FUNZIONI CREATE---------

	public function removeRack($id){
		//return Rack::where('id', $id)->delete();
		try {
			$conn = mysqli_connect("localhost", "root", "", "bike_db");
			$sql = "DELETE FROM racks WHERE id='$id'";
			mysqli_query($conn, $sql);
			mysqli_close($conn);
		} catch (PDOException $e) {
			echo "ERROR, failed to delete Rack";
		}
	}

	// //AGGIUNGI UNA NUOVA RASTRELLIERA
	// public function addRack($request){
	// 	$rack = new Rack;
	// 	$rack->id = $request -> input('id');
	// 	$rack->capacity = $request -> input('capacity');
	// 	$rack->location_description = $request -> input('location_description');
	// 	$rack->save();
	// 	return $rack;
	// }

	// //MODIFICA DESCRIZIONE LOCALITA' RASTRELLIERA
	// public function modLocalDesc($id, $request){
	// $rack = Rack::findOrFail($id);
	// $rack -> location_description = $request -> input('location_description');
	// $rack->update();
	// return $rack;
	// }

	// //MODIFICA CAPACITA' RASTRELLIERA
	// public function modCapacity($id, $request){
	// $rack = rack::findOrFail($id);
	// $rack -> capacity = $request -> input('capacity');
	// $rack->update();
	// return $rack;
	// }

}
