<?php  

namespace App\Repositories;

use App\Reservation;
use Illuminate\Support\Facades\DB;

class ReservationRepository {

	public function findActiveReservation($user_id)
	{
		return Reservation::where('user_id', '=', $user_id)->get();
	}


}