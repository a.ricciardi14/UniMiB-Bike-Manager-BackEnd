<?php

namespace App\Repositories;

use App\Bike;
use App\Rental;
use App\Rack;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;

//avgSpeed in meters/second
define('AVG_SPEED', 2.77);
//avgEmission in g/km
define ('AVG_EMISSION', 0.095);

class RentalRepository {


	public function findAllRentals()
	{
		return Rental::all();
	}

	public function findRentalsByUser($user_id)
	{
		return Rental::where('user_id', '=' , $user_id)->with('bike','startRack','endRack')->orderBy('started_on', 'desc')->get();
	}

	public function findActiveRental($user_id)
	{
		return Rental::where([['user_id', '=', $user_id],['completed_on', '=', null]])->with('bike','startRack')->get()->first();
	}

	public function startRental($request)
	{
		$bike = Bike::findOrFail($request->input('bike_id'));
		$bike_id = $bike->id;

		//ottengo id utente che inizia noleggio
		$user = User::findOrFail($request->input('user_id'));
		$user_id = $user->id;

		//$user_id = $request->input('user_id');
		$start_rack_id = $bike->rack->id;
		$bike_state = $bike->BikeState->id;
		if($bike_state == DISPONIBILE){
			$rental = new Rental;
			$rental->started_on = date('Y-m-d H:i:s');
			$rental->completed_on = null;
			$rental->bike_id = $bike_id;
			$rental->user_id = $user_id;
			$rental->start_rack_id = $start_rack_id;
			$rental->end_rack_id = null;
			$rental->co2_emission = null;
			$rental->save();

			$bike->bike_state_id = NOLEGGIATA;
			$bike->save();

			$user->user_state_id = NOLEGGIO_IN_CORSO;
			$user->save();

			$lastId = $rental->id;
				return Rental::where('id', $lastId)->with('bike','startRack')->get()->first();
		}
		else
			 throw new \App\Exceptions\CustomException('Errore');
	}

	/*private function codeGenerator(){
		$digits = 4;
    	$i = 0;
    	$code = '';
    		while($i < $digits){
        	$code .= mt_rand(0, 9);
        	$i++;
    	}
    return $code;
	}*/


	public function endRental($request,$rental_id)
	{
		$rental = Rental::where('id', $rental_id)->with('bike','startRack','endRack')->get()->first();
		$rental->end_rack_id = $request->rack_id;
		$rental->completed_on = date('Y-m-d H:i:s');
		
		//Calcolo emissioni CO2 evitate
		$dateStart = $rental->started_on;
		$dateTimeStart = new DateTime($dateStart);
		$dateEnd = $rental->completed_on;
		$dateTimeEnd = new DateTime($dateEnd);
		$diffInSeconds = $dateTimeEnd->getTimestamp() - $dateTimeStart->getTimestamp();
		$avoidedEmissions = $diffInSeconds*AVG_SPEED*AVG_EMISSION;
		$rental->co2_emission = $avoidedEmissions;

		$rental->save();

		$bike_id = $rental->bike_id;
		$bike = Bike::where('id', $bike_id)->get()->first();
		$bike->bike_state_id = DISPONIBILE;
		$bike->rack_id = $rental->end_rack_id;
		$bike->save();

		$user = User::findOrFail($rental->user_id);
		$user->user_state_id = ABILITATO;
		$user->save();


		//$code = self::codeGenerator();
		//$bike->unlock_code = $code;


		return Rental::where('id', $rental_id)->with('bike','startRack','endRack')->get()->first();
	}

}
