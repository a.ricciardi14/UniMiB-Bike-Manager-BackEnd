<?php

namespace App\Repositories;

use App\Bike;
use Illuminate\Support\Facades\DB;

class BikeRepository {


	public function findBikeById($id){
		return Bike::with('rack','bikeState')->findOrFail($id);
	}

	//-------FUNZIONI CREATE---------

	public function findBikeList(){
		return Bike::with('rack', 'bikeState')->get();
	}

	public function removeBike($id){
		return Bike::where('id', $id)->delete();
	}


	// //MODIFICA DISPONIBILITA' BICICLETTA
	// public function modDisp($id, $request){
	//
	// $bike = Bike::findOrFail($id);
	// $bike -> bike_state_id = $request -> input('bike_state_id');
	// $bike->update();
	// return $bike;
	// }
	//
	// //MODIFICA CODICE DI SBLOCCO DI UNA BICICLETTA
	// public function modUnCode($id, $request){
	//
	// 	$bike = Bike::findOrFail($id);
	// 	$bike-> unlock_code = $request->input('unlock_code');
	// 	$bike->update();
	// 	return $bike;
	// }
	//
	// //AGGIUNGI UNA NUOVA BICICLETTA
	// public function addBike($request){
	// 	$bike = new Bike;
	// 	$bike->id = $request -> input('id');
	// 	$bike->unlock_code = $request -> input('unlock_code');
	// 	$bike->rack_id = $request -> input('rack_id');
	// 	$bike->bike_state_id = 1;
	// 	$bike->save();
	// 	return $bike;
	// }

}
