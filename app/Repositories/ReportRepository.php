<?php

namespace App\Repositories;

use App\Report;
use Illuminate\Support\Facades\DB;

class ReportRepository {

	//public function storeReport($request,$id)
	public function storeReport($request)
	{
		$report = new Report;
		$report->description = $request->input('description');
		$report->bike_id = $request->input('bike_id');
		//$report->user_id = $id;
		$report->user_id = $request->input('user_id');
		$report->image = $request->input('image');
    $report->save();
    return $report;
	}

	public function getImage($id)
	{
		$report = Report::where('id', $id)->first();
		try {
                $image = $report->image;
				echo '<img src="data:image/jpeg;base64,'.$image.'"/>';
            }catch (\Exception $e){
                echo 'No image available';
            }
	}

	//-------FUNZIONI CREATE---------

	public function findAllReports(){
		return Report::get();
	}

}
