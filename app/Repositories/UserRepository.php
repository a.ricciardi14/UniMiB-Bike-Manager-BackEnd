<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\DB;

class UserRepository {

	public function findIdByEmail($email){
		return User::select('id', 'email', 'user_state_id')->where('email', $email)->first();
	}

	public function authenticateUser($request){
	    function get_http_response()
        {
            //The URL with parameters / query string.
            $url = 'https://bridge.si.unimib.it/cas_all_unimib/login';

            // create curl resource
            $ch = curl_init();

            // set url
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);

            return curl_exec($ch);
        }

		function get_headerValue_from_curl_response($response, $headerKey)
		{
			$headers = array();
			$header_text = substr($response, 0, strpos($response, "\r\n\r\n"));
			foreach (explode("\r\n", $header_text) as $i => $line)
				if ($i === 0)
					$headers['http_code'] = $line;
				else
				{
					list ($key, $value) = explode(': ', $line);
					$headers[$key] = $value;
				}
			$jSession = explode(';', $headers[$headerKey])[0];
			return $jSession;
		}

		function get_ltValue_from_body($body)
		{
			$pos = strpos($body, 'name="lt"')+17;
			$lt = substr($body, $pos, 76);
			return $lt;
		}

		function postRequest($parameters){
			//preparo la chiamata POST
            $url = 'https://bridge.si.unimib.it/cas_all_unimib/login';

            $data =  'username='.$parameters[0].'&password='.$parameters[1].
                '&submit=Login&lt='.$parameters[3].'&_eventId=submit';

            $options = array(
                'http' => array(
                    'header'=>array("Content-type: application/x-www-form-urlencoded",
                        "Cookie: ".$parameters[2],),
                    'method'  => 'POST',
                    'content' => $data
                )
            );
			//Invio la chiamata e salvo il contenuto
           $context  = stream_context_create($options);

           //controllo presenza cookie CASTGC se login effettuato
		   //Questo cookie viene settato solo se il login ha avuto successo, quindi è fondamentale cercarlo
            try {
                get_headers($url, 1, $context)['Set-Cookie'][1];
                return true;
            }catch (\Exception $e){
                return false;
            }
        }

		//---------		Inizio procedura login	-------------//
        $response = get_http_response();
        $ch = curl_init();
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $body = substr($response, $header_size);

		//	Per invoca correttamente la procedura di login ho bisogno delle seguenti
		// 	4 variabili
		$jSession = get_headerValue_from_curl_response($response, 'Set-Cookie');
		$lt = get_ltValue_from_body($body);
		$email = $request->input('email');
		$password = $request->input('password');
		
        $parameters = array($email, $password, $jSession, $lt);
        $loginIsValid = postRequest($parameters);

		if ($loginIsValid)
			return response()->json("Account valid", 200);
		else
			return response()->json("Unauthorized", 401);

	}

	public function addUser($email){
		DB::table('users')->insert(['email' => $email, 'user_state_id' => 1]);
		return User::select('id', 'email')->where('email', $email)->first();
	}
}
