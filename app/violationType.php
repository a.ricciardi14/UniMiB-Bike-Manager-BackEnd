<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViolationType extends Model
{
    public function violations() 
 	{
        return $this->hasMany('App\Violation');
    }

}