<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{

	protected $hidden = ['pivot'];
	
    public function racks() 
 	{
        return $this->belongsToMany('App\Rack');
    }

}
