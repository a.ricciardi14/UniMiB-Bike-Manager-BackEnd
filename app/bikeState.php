<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BikeState extends Model
{
	protected $hidden = ['id'];
	
    public function bikes() 
 	{
        return $this->hasMany('App\Bike');
    }
}
