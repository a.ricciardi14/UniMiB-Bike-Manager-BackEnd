<?php

namespace App\Http\Controllers;

use App\Repositories\ReportRepository;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    private $reportRepository;

    public function __construct(ReportRepository $reportRepository)
    {
        $this->reportRepository = $reportRepository;
    }

    public function store(Request $request)
    {
    	/*
		$id=1;
        return response() ->json(['report' => $this -> reportRepository -> storeReport($request,$id)], 200);
		*/
		return response() ->json(['report' => $this -> reportRepository -> storeReport($request)], 200);
    }

	public function getImage($id)
    {
    	return $this -> reportRepository -> getImage($id);
    }

  public function getAllReports(){
    return response() ->json(['reports' => $this -> reportRepository -> findAllReports()], 200);
  }

}
