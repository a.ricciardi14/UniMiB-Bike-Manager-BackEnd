<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{

	private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
	public function index(Request $request){
       if($request->input('email')){
            $email = $request->input('email');
            return response() ->json($this -> userRepository -> findIdByEmail($email), 200);
        }
	}
	
	public function authUser(Request $request){
        
		return $this -> userRepository -> authenticateUser($request);
    }
	
	public function storeUser(Request $request){
        $email = $request->input('email'); 
		return $this -> userRepository -> addUser($email);
    }

}