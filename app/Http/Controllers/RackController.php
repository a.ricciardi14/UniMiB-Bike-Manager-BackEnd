<?php

namespace App\Http\Controllers;

use App\Repositories\PositionService;
use App\Repositories\RackRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class RackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $rackRepository;
    private $positionService;

    public function __construct(RackRepository $rackRepository, PositionService $positionService)
    {
        $this->rackRepository = $rackRepository;
        $this->positionService = $positionService;
    }

    public function index(Request $request){
       if($request->input('available_stands')){
            $available_stands = $request->input('available_stands');
            return response() ->json(['racks' => $this -> rackRepository -> findRacksByAvailableStands($available_stands)], 200);
        }
		    else if($request->input('available_bikes')){
            $available_bikes = $request->input('available_bikes');
            return response() ->json(['racks' => $this -> rackRepository -> findRacksByAvailableBikes($available_bikes)], 200);
        }
        else if($request->input('building_id')){
            $building_id = $request->input('building_id');
            return response() ->json(['racks' => $this -> rackRepository -> findRacksByBuildingId($building_id)], 200);
        }
        else if($request->has('lat', 'long', 'max_distance')){
            $lat = $request->lat;
            $long = $request->long;
            $max_distance = $request->max_distance;
            $array = $this->rackRepository->FindRacksList();
            return response() ->json(['racks' => $this -> positionService -> calculateMaxDist($array,$lat,$long,$max_distance)], 200);
        }
        else if($request->has('lat','long')){
            $lat = $request->lat;
            $long = $request->long;
            $array = $this->rackRepository->FindRacksList();
            return response() ->json(['racks' => $this -> positionService -> calculate($array,$lat,$long)], 200);
        }
        else
            return response() ->json(['racks' => $this -> rackRepository -> findAllRacks()], 200);


    }

    public function getRack($id) {
        return response() ->json(['rack' => $this -> rackRepository -> findRackById($id)], 200);
    }

    //-------FUNZIONI CREATE---------

    public function deleteRack($id){
			return response() -> json(['rack' => $this -> rackRepository -> removeRack($id)], 200);
		}

    // public function storeRack($request){
		// 	return response() -> json(['bike' => $this -> rackRepository -> addRack($request)], 200);
		// }

    // public function changeLocalDesc($id, $request){
		// 	return response() -> json(['bike' => $this -> rackRepository -> modLocalDesc($id, $request)], 200);
		// }
    
    // public function changeCapacity($id, $request){
		// 	return response() -> json(['bike' => $this -> rackRepository -> modCapacity($id, $request)], 200);
		// }

}
