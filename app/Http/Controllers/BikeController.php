<?php

namespace App\Http\Controllers;

use App\Repositories\BikeRepository;
use Illuminate\Http\Request;

class BikeController extends Controller
{

	private $bikeRepository;

    public function __construct(BikeRepository $bikeRepository)
    {
        $this->bikeRepository = $bikeRepository;
    }

    public function getBike($id) {
        return response() ->json(['bike' => $this -> bikeRepository -> findBikeByid($id)], 200);
    }


		//-------FUNZIONI CREATE---------

		public function getBikeList(){
			return response() -> json(['bikes' => $this -> bikeRepository -> findBikeList()], 200);
		}

		public function deleteBike($id){
			return response() -> json(['bike' => $this -> bikeRepository -> removeBike($id)], 200);
		}


		// public function changeDisp($id, $request){
		// 	return response() -> json(['bike' => $this -> bikeRepository -> modDisp($id, $request)], 200);
		// }

		// public function changeUnCode($id, $request){
		// 	return response() -> json(['bike' => $this -> bikeRepository -> modUnCode($unCode, $id)], 200);
		// }

		// public function storeBike($request){
		// 	return response() -> json(['bike' => $this -> bikeRepository -> addBiketoRack($request)], 200);
		// }


}
