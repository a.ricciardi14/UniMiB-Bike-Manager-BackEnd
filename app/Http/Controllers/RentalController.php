<?php

namespace App\Http\Controllers;

use App\Repositories\RentalRepository;
use Illuminate\Http\Request;
use DateTime;

class RentalController extends Controller
{
    private $rentalRepository;

    public function __construct(RentalRepository $rentalRepository)
    {
        $this->rentalRepository = $rentalRepository;
    }

    public function index(Request $request)
    {
        //$user_id = 4;
		$user_id = $request->input('id');
    	if($request->input('active')){
            $active = $request->input('active');
            if($active == 'yes'){
            	return response() -> json(['rental' => $this -> rentalRepository -> findActiveRental($user_id)], 200);
            }
        
		}
		else
		{
			return response() ->json(['rentals' => $this -> rentalRepository -> findRentalsByUser($user_id)], 200);
		} 

    }

    public function storeRental(Request $request){
        //$user_id=11;
        //return response() -> json(['rental' => $this -> rentalRepository -> startRental($request,$user_id)], 200);
		return response() -> json(['rental' => $this -> rentalRepository -> startRental($request)], 200);
    }

    public function closeRental(Request $request,$rental_id){
        return response() -> json(['rental' => $this -> rentalRepository -> endRental($request,$rental_id)], 200);
    }

}
