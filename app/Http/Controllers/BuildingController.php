<?php

namespace App\Http\Controllers;

use App\Repositories\BuildingRepository;
use Illuminate\Http\Request;

class BuildingController extends Controller
{
    private $buildingRepository;

    public function __construct(BuildingRepository $buildingRepository)
    {
        $this->buildingRepository = $buildingRepository;
    }

    public function index()
    {
        return response() ->json(['buildings' => $this -> buildingRepository -> findAllBuildings()], 200);
    }
}
