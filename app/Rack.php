<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Rack extends Model
{
	protected $appends = ['available_bikes'];
	//protected $hidden = ['pivot', 'capacity'];	

    public function bikes() 
 	{
        return $this->hasMany('App\Bike');
    }

    public function buildings() 
 	{
        return $this->belongsToMany('App\Building');
    }

    public function getAvailableBikesAttribute()
	{	
	//nuova funziona per calcolare le bici disponibili, tenendo conto solo di quelle con state=1 
		$available_bikes = DB::table('racks')
		->join('bikes', 'bikes.rack_id', '=', 'racks.id')
		->where('bikes.bike_state_id', '=', 1)
		->where('racks.id', '=', $this->id)
		->select('bikes.id')
		->groupBy('racks.id')
		->count();

		return $available_bikes ;
		
	}
	
}

/*

SELECT COUNT(bikes.id) AS avaiable_bikes
FROM bikes, racks 
WHERE bikes.rack_id=racks.id AND bike_state_id=1 
GROUP BY rack_id


public function getAvailableBikesAttribute()
	{
		
		return $this->capacity - $this->available_stands ;
		 
		
	}
	
*/