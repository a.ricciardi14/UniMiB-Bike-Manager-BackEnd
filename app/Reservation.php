<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
	protected $hidden = ['pivot', 'user_id'];

    public function bike()
    {
    	return $this->belongsTo('App\Bike');
    }

     public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
