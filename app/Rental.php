<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
    public $timestamps = false;


    public function startRack() 
 	{
        return $this->belongsTo('App\Rack', 'start_rack_id');
    }

    public function endRack() 
 	{
        return $this->belongsTo('App\Rack', 'end_rack_id');
    }

     public function bike()
    {
    	return $this->belongsTo('App\Bike');
    }

     public function user()
    {
        return $this->belongsTo('App\User');
    }
}
