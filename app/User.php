<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

define('ABILITATO', 1);
define('PRENOTAZIONE_IN _ORSO', 2);
define('NOLEGGIO_IN_CORSO', 3);
define('SOSPESO', 4);


class User extends Model
{
	protected $fillable = ['id'];
	public $timestamps = false;
    public function userState() 
    {
        return $this->belongsTo('App\UserState');
    }

     public function bikesRentals() 
    {
        return $this->belongsToMany('App\Bike', 'rentals')->withPivot('id','started_on', 'completed_on', 'start_rack_id', 'end_rack_id');
    }

    public function bikeReports() 
    {
        return $this->belongsToMany('App\Bike', 'reports')->withPivot('description','created_on');
    }

    public function bikeReservations() 
    {
        return $this->belongsToMany('App\Bike', 'reservations')->withPivot('id','reserved_on');
    }

    public function suspensions() 
    {
        return $this->hasMany('App\Suspension');
    }

    public function violations() 
    {
        return $this->hasMany('App\Violation');
    }
}
