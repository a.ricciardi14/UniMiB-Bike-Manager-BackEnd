<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

define('DISPONIBILE', 1);
define('PRENOTATA', 2);
define('NOLEGGIATA', 3);
define('SMARRITA', 4);
define('MANUTENZIONE', 5);

class Bike extends Model
{

    public $timestamps = false;
    protected $hidden = ['rack_id','bike_state_id'];

	public function bikeState() 
 	{
        return $this->belongsTo('App\BikeState');
    }

    public function rack() 
 	{
        return $this->belongsTo('App\Rack');
    }

    public function userReports()
    {
    	return $this->belongsToMany('App\User', 'reports')->withPivot('description','created_on');
    }

     public function userReservations()
    {
    	return $this->belongsToMany('App\User', 'reservations')->withPivot('id','reserved_on');
    }

     public function userRentals()
    {
    	return $this->belongsToMany('App\User', 'rentals')->withPivot('id','started_on', 'completed_on', 'start_rack_id', 'end_rack_id');
    }
    
}
