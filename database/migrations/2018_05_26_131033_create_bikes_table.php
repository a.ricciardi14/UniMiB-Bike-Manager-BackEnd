<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bikes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unlock_code');
            $table->unsignedInteger('rack_id');
            $table->unsignedInteger('bike_state_id');
            $table->foreign('rack_id')->references('id')->on('racks');
            $table->foreign('bike_state_id')->references('id')->on('bike_states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bikes');
    }
}
