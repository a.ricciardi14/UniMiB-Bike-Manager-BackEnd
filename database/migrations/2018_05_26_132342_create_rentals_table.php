<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rentals', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('started_on');
            $table->dateTime('completed_on');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('bike_id');
            $table->unsignedInteger('start_rack_id');
            $table->unsignedInteger('end_rack_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('bike_id')->references('id')->on('bikes');
            $table->foreign('start_rack_id')->references('id')->on('racks');
            $table->foreign('end_rack_id')->references('id')->on('racks');
			$table->unsignedInteger('co2_emission')->nullable();
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rentals');
    }
}
