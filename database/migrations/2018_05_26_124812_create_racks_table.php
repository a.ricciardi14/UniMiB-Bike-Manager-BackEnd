<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('racks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('capacity');
            $table->integer('available_stands');
            $table->decimal('latitude', 12, 9);
            $table->decimal('longitude', 12, 9);
            $table->string('address_locality', 50);
            $table->mediumText('street_address');
            $table->mediumText('location_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('racks');
    }
}
