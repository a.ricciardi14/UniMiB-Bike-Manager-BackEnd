<?php

use Illuminate\Database\Seeder;

class RacksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('racks')->insert([
        	['id'=>1, 'capacity'=> 10, 'available_stands'=> 8, 'latitude'=> 45.515156,'longitude'=> 9.212419,'address_locality'=> 'Milano','street_address' => 'Via Luigi Emanueli 7','location_description' => 'Rastrelliera U1-U2'],
            ['id'=>2,'capacity'=> 14, 'available_stands'=> 6,'latitude'=> 45.5184464,'longitude'=> 9.2120114,'address_locality'=> 'Milano','street_address' => 'Via Padre Gerardo Beccaro 2','location_description' => 'Rastrelliera U6'],
            ['id'=>3,'capacity'=> 20, 'available_stands'=> 10,'latitude'=> 45.511256,'longitude'=> 9.2092299,'address_locality'=> 'Milano','street_address' => 'Viale dell innvazione 10','location_description' => 'Rastrelliera U9'],
            ['id'=>4,'capacity'=> 16, 'available_stands'=> 10,'latitude'=> 45.5236924,'longitude'=> 9.217302,'address_locality'=> 'Milano','street_address' => 'Viale Sarca 336','location_description' => 'Rastrelliera U14'],
            ['id'=>5,'capacity'=> 14, 'available_stands'=> 10,'latitude'=> 45.5171288,'longitude'=> 9.2111541,'address_locality'=> 'Milano','street_address' => 'Via Padre Gerardo Beccaro 2','location_description' => 'Rastrelliera U7']

        ]);
        
    }
}
