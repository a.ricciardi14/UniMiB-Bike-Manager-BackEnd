<?php

use Illuminate\Database\Seeder;

class BuildingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buildings')->insert([
        	['id'=> 1, 'name'=> 'edificio U1'],
        	['id'=> 2, 'name'=> 'edificio U2'],
        	['id'=> 3, 'name'=> 'edificio U3'],
        	['id'=> 4, 'name'=> 'edificio U4'],
        	['id'=> 5, 'name'=> 'edificio U6'],
        	['id'=> 6, 'name'=> 'edificio U7'],
            ['id'=> 7, 'name'=> 'edificio U9'],
            ['id'=> 8, 'name'=> 'edificio u14']
        ]);
    }
}
