<?php

use Illuminate\Database\Seeder;

class BuildingRackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
         public function run()
    {
        DB::table('building_rack')->insert([
        	['id'=> 1, 'rack_id'=> 1, 'building_id'=> 1],
        	['id'=> 2, 'rack_id'=> 1, 'building_id'=> 2],
        	['id'=> 3, 'rack_id'=> 2, 'building_id'=> 5],
        	['id'=> 4, 'rack_id'=> 3, 'building_id'=> 7],
        	['id'=> 5, 'rack_id'=> 4, 'building_id'=> 8],
        	['id'=> 6, 'rack_id'=> 5, 'building_id'=> 6]
        ]);
    }
}