<?php

use Illuminate\Database\Seeder;

class BikesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        $faker = Faker\Factory::create();
        DB::table('bikes')->insert([
        	['id'=>1, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 1, 'bike_state_id'=> 2],
        	['id'=>2, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 1, 'bike_state_id'=> 1],
        	['id'=>3, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 2, 'bike_state_id'=> 1],
        	['id'=>4, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 2, 'bike_state_id'=> 1],
        	['id'=>5, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 2, 'bike_state_id'=> 2],
        	['id'=>6, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 2, 'bike_state_id'=> 1],
        	['id'=>7, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 2, 'bike_state_id'=> 1],
        	['id'=>8, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 2, 'bike_state_id'=> 2],
        	['id'=>9, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 2, 'bike_state_id'=> 1],
        	['id'=>10, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 2, 'bike_state_id'=> 2],
        	['id'=>11, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 3, 'bike_state_id'=> 1],
        	['id'=>12, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 3, 'bike_state_id'=> 1],
        	['id'=>13, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 3, 'bike_state_id'=> 5],
        	['id'=>14, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 3, 'bike_state_id'=> 1],
        	['id'=>15, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 3, 'bike_state_id'=> 1],
        	['id'=>16, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 3, 'bike_state_id'=> 2],
        	['id'=>17, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 3, 'bike_state_id'=> 5],
        	['id'=>18, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 3, 'bike_state_id'=> 1],
        	['id'=>19, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 3, 'bike_state_id'=> 4],
        	['id'=>20, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 3, 'bike_state_id'=> 1],
        	['id'=>21, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 4, 'bike_state_id'=> 1],
        	['id'=>22, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 4, 'bike_state_id'=> 5],
        	['id'=>23, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 4, 'bike_state_id'=> 1],
        	['id'=>24, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 4, 'bike_state_id'=> 1],
        	['id'=>25, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 4, 'bike_state_id'=> 1],
        	['id'=>26, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 4, 'bike_state_id'=> 4],
        	['id'=>27, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 5, 'bike_state_id'=> 1],
        	['id'=>28, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 5, 'bike_state_id'=> 1],
        	['id'=>29, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 5, 'bike_state_id'=> 5],
        	['id'=>30, 'unlock_code'=> $faker->unique()->numberBetween($min = 1000, $max = 9999), 'rack_id'=> 5, 'bike_state_id'=> 1]
        ]);
    }
}
