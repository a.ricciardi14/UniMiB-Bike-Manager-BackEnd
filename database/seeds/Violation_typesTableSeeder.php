<?php

use Illuminate\Database\Seeder;

class ViolationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('violation_types')->insert([
        	['id'=> 1, 'description'=> 'Consegna in ritardo', 'ban_duration'=> 2],
        	['id'=> 2, 'description'=> 'Bici Danneggiata', 'ban_duration'=> 5],
        	['id'=> 3, 'description'=> 'Noleggio non terminato', 'ban_duration'=>10]

        ]);
    }
}
