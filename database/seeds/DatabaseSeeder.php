<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
	public function run()
		{
    		$this->call([
    		BikeStatesTableSeeder::class,
    		UserStatesTableSeeder::class,
    		RacksTableSeeder::class,
    		BuildingsTableSeeder::class,
    		BikesTableSeeder::class,
        	BuildingRackTableSeeder::class,
        	UsersTableSeeder::class,
        	ViolationTypesTableSeeder::class
    		]);
    	}	
}
