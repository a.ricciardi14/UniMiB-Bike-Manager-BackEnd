<?php

use Illuminate\Database\Seeder;

class UserStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_states')->insert([
        	['id'=> 1, 'description'=> 'Abilitato'],
        	['id'=> 2, 'description'=> 'Prenotazione in corso'],
        	['id'=> 3, 'description'=> 'Noleggio in corso'],
        	['id'=> 4, 'description'=> 'Sospeso']

        ]);
    }
}
