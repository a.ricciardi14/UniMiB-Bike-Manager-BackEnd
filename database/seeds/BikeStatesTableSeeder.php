<?php

use Illuminate\Database\Seeder;

class BikeStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bike_states')->insert([
            ['id'=>1, 'description'=> 'Disponibile'],
            ['id'=>2, 'description'=> 'Prenotata'],
            ['id'=>3, 'description'=> 'Noleggiata'],
            ['id'=>4, 'description'=> 'Smarrita'],
            ['id'=>5, 'description'=> 'Manutenzione'],
        ]);
    }
}
