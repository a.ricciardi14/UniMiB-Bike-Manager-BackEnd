<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	['id'=>1, 'email'=> 'guest@unimib.it', 'user_state_id'=>1],
        	['id'=>2, 'email'=> str_random(20).'@unimib.it', 'user_state_id'=>1],
        	['id'=>3, 'email'=> str_random(20).'@unimib.it', 'user_state_id'=>1],
        	['id'=>4, 'email'=> str_random(20).'@unimib.it', 'user_state_id'=>1],
        	['id'=>5, 'email'=> str_random(20).'@unimib.it', 'user_state_id'=>1],
        	['id'=>6, 'email'=> str_random(20).'@unimib.it', 'user_state_id'=>1],
        	['id'=>7, 'email'=> str_random(20).'@unimib.it', 'user_state_id'=>1],
        	['id'=>8, 'email'=> str_random(20).'@unimib.it', 'user_state_id'=>1],
        	['id'=>9, 'email'=> str_random(20).'@unimib.it', 'user_state_id'=>1],
        	['id'=>10, 'email'=> str_random(20).'@unimib.it', 'user_state_id'=>1],

        ]);
    }
}
